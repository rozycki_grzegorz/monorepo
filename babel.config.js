module.exports = {
    babelrcRoots: [
        // '.',
        // './packages/*',
    ],
    presets: [
        [
            '@babel/env',
            {
                modules: false,
                useBuiltIns: 'usage',
                targets: {
                    browsers: ['> 1%'],
                },
            },
        ],
        '@babel/react',
    ],
    plugins: [
        '@babel/plugin-proposal-class-properties',
        '@babel/plugin-proposal-object-rest-spread',
        '@babel/plugin-transform-object-assign',
        '@babel/plugin-transform-runtime',
    ],
};
